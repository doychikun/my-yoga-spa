export const yogaTypes = {
    DEFAULT_VINYASA_FLOW_YOGA: "yoga flow",
    HATHA_YOGA: "hatha yoga",
    ASHTANGA_YOGA: "ashtanga yoga",
    YIN_YOGA: "yin yoga",
    MINDFULNESS_YOGA: "mindfulness yoga"
};